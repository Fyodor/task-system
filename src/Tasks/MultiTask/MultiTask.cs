﻿using System;

namespace TaskSystem
{
    abstract class TaskBaseWithFullControl : TaskBase, ITaskFullControl
    {
        protected TaskBaseWithFullControl(ITaskSet taskSet, ITaskContext parentContext) 
            : base(taskSet, parentContext)
        {
        }

        #region ITaskFullControl

        public ITaskFullControl WaitFor(params ITaskOrder[] tasks)
        {
            WaitTask task = new WaitTask(TaskSet, Context);

            int order = task.Order;
            for (int i = 0; i < tasks.Length; ++i)
            {
                order = Math.Max(order, tasks[i].Order);
            }

            task.MakeAfter(order);
            return Append(task);
        }

        public ITaskFullControl If(ITaskResult<bool> criterion)
        {
            return Append(new IfTask(TaskSet, Context, criterion, true));
        }

        public ITaskFullControl IfNot(ITaskResult<bool> criterion)
        {
            return Append(new IfTask(TaskSet, Context, criterion, false));
        }

        public ITaskFullControl SubSaga(Saga subSaga)
        {
            return Append(new SagaTask(subSaga));
        }

        #endregion
    }
}