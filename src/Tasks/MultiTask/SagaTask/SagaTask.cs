﻿using System;
using System.Threading.Tasks;

namespace TaskSystem
{
    abstract class Saga : ITaskFullControl
    {
        private ITaskFullControl mControl;
        
        public abstract ITaskResult<bool> Process();
        public abstract void Rollback();

        public void Setup(ITaskFullControl control)
        {
            mControl = control;
        }

        protected Constant<TResult> Constant<TResult>(TResult constant)
        {
            return new Constant<TResult>(Order + 1, constant);
        }

        #region ITaskFullControl

        public int Order
        {
            get { return mControl.Order; }
        }

        public ITaskFullControl WaitFor(params ITaskOrder[] tasks)
        {
            return mControl.WaitFor(tasks);
        }

        public ITaskFullControl If(ITaskResult<bool> criterion)
        {
            return mControl.If(criterion);
        }

        public ITaskFullControl IfNot(ITaskResult<bool> criterion)
        {
            return mControl.IfNot(criterion);
        }

        public ITaskFullControl SubSaga(Saga subSaga)
        {
            return mControl.SubSaga(subSaga);
        }

        public ITask<TResult> NextTask<TResult>(Task<TResult> task)
        {
            return mControl.NextTask(task);
        }

        public ITask NextAction(Action<ITaskContext> action)
        {
            return mControl.NextAction(action);
        }

        public ITask NextAction<TParam1>(Action<ITaskContext, TParam1> action, TParam1 param1)
        {
            return mControl.NextAction(action, param1);
        }

        public ITask<TResult> NextFunction<TResult>(Func<ITaskContext, TResult> function)
        {
            return mControl.NextFunction(function);
        }

        public ITask<TResult> NextFunction<TParam1, TResult>(Func<ITaskContext, TParam1, TResult> function, TParam1 param1)
        {
            return mControl.NextFunction(function, param1);
        }

        public ITask<TResult> NextFunction<TParam1, TParam2, TResult>(Func<ITaskContext, TParam1, TParam2, TResult> function, TParam1 param1, TParam2 param2)
        {
            return mControl.NextFunction(function, param1, param2);
        }

        #endregion
    }
    
    sealed class SagaTask : TaskBaseWithFullControl
    {
        private class TaskContext : ITaskContext
        {
            void ITaskContext.Log(string message, params object[] list)
            {
                Console.WriteLine(message, list);
            }
        }
        
        private readonly TaskSet mTaskSet;
        private readonly Saga mSaga;

        private SagaTask(TaskSet taskSet, ITaskContext context)
            : base(taskSet, context)
        {
            mTaskSet = taskSet;
        }

        public SagaTask(Saga saga)
            : this(new TaskSet(), new TaskContext())
        {
            saga.Setup(this);
            mSaga = saga;
        }

        public sealed override Task<bool> DoWork()
        {
            return Process();
        }

        public sealed override Task Rollback()
        {
            return Revert();
        }

        private async Task<bool> Process()
        {
            ITaskResult<bool> res = await Task.Factory.StartNew(mSaga.Process);
            await mTaskSet.Process();
            return res.Result;
        }

        private async Task Revert()
        {
            await mTaskSet.Rollback();
            await Task.Factory.StartNew(mSaga.Rollback);
        }

        public async Task<bool> RunAsRoot()
        {
            if (!await DoWork())
            {
                await Rollback();
                return false;
            }
            return true;
        }
    }
}
