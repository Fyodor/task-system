﻿namespace TaskSystem
{
    sealed class WaitTask : TaskBaseWithFullControl
    {
        public WaitTask(ITaskSet taskSet, ITaskContext parentContext) 
            : base(taskSet, parentContext)
        {
        }
    }
}