﻿using System.Threading.Tasks;

namespace TaskSystem
{
    sealed class IfTask : TaskBaseWithFullControl
    {
        class ConditionalTask : TaskBase
        {
            private readonly TaskBase mCore;
            private readonly ITaskResult<bool> mCriterion;
            private readonly bool mIsTrue;

            private bool mSomethingDone = false;

            public ConditionalTask(TaskBase core, ITaskResult<bool> criterion, bool isTrue) 
                : base((core as ITaskBaseView).TaskSet, (core as ITaskBaseView).Context)
            {
                mCore = core;
                mCriterion = criterion;
                mIsTrue = isTrue;
                MakeAfter(core.Order - 1);
            }

            public override Task<bool> DoWork()
            {
                if (mCriterion.Result == mIsTrue)
                {
                    mSomethingDone = true;
                    return mCore.DoWork();
                }
                return Task.FromResult(true);
            }

            public override Task Rollback()
            {
                if (mSomethingDone)
                {
                    return mCore.Rollback();
                }
                return Task.FromResult<object>(null);
            }
        }

        private class TaskSetter : ITaskSet
        {
            private readonly ITaskSet mCore;
            private readonly ITaskResult<bool> mCriterion;
            private readonly bool mIsTrue;

            private IfTask mOwner;

            public TaskSetter(ITaskSet core, ITaskResult<bool> criterion, bool isTrue)
            {
                mCore = core;
                mCriterion = criterion;
                mIsTrue = isTrue;
            }

            public void Setup(IfTask owner)
            {
                mOwner = owner;
            }

            void ITaskSet.Add(TaskBase task)
            {
                (task as ITaskBaseView).MakeAfter(mOwner.Order - 1);
                ConditionalTask conditionalTask = new ConditionalTask(task, mCriterion, mIsTrue);
                mCore.Add(conditionalTask);
            }
        }

        private readonly TaskSetter mTaskSetter;

        private IfTask(TaskSetter taskSetter, ITaskContext parentContext)
            : base(taskSetter, parentContext)
        {
            taskSetter.Setup(this);
            mTaskSetter = taskSetter;
        }

        public IfTask(ITaskSet taskSet, ITaskContext parentContext, ITaskResult<bool> criterion, bool isTrue)
            : this(new TaskSetter(taskSet, criterion, isTrue), parentContext)
        {
            MakeAfter(criterion.Order);
        }
    }
}