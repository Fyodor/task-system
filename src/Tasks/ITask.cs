﻿using System.Threading.Tasks;

namespace TaskSystem
{
    interface IResult<TResult>
    {
        TResult Result { get; }
    }

    interface ITaskOrder
    {
        int Order { get; }
    }

    interface ITaskResult : ITaskOrder
    {
    }

    interface ITaskResult<TResult> : IResult<TResult>, ITaskOrder
    {
    }

    interface ITaskControl : ITaskOrder
    {
        ITask<TResult> NextTask<TResult>(Task<TResult> task);

        ITask NextAction(System.Action<ITaskContext> action);
        ITask NextAction<TParam1>(System.Action<ITaskContext, TParam1> action, TParam1 param1);

        ITask<TResult> NextFunction<TResult>(System.Func<ITaskContext, TResult> function);
        ITask<TResult> NextFunction<TParam1, TResult>(System.Func<ITaskContext, TParam1, TResult> function, TParam1 param1);
        ITask<TResult> NextFunction<TParam1, TParam2, TResult>(System.Func<ITaskContext, TParam1, TParam2, TResult> function, TParam1 param1, TParam2 param2);
    }

    interface ITaskFullControl : ITaskControl
    {
        ITaskFullControl WaitFor(params ITaskOrder[] tasks);
        ITaskFullControl If(ITaskResult<bool> criterion);
        ITaskFullControl IfNot(ITaskResult<bool> criterion);

        ITaskFullControl SubSaga(Saga subSaga);
    }

    interface ITask : ITaskResult, ITaskControl
    {
    }

    interface ITask<TResult> : ITaskResult<TResult>, ITaskControl
    {
    }
}