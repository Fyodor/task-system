﻿using System;
using System.Threading.Tasks;

namespace TaskSystem
{
    /// <summary>
    /// Базовый класс для всех задач
    /// </summary>
    abstract class TaskBase : ITaskResult, ITaskControl, TaskBase.ITaskBaseView
    {
        protected interface ITaskBaseView
        {
            void MakeAfter(int order);
            ITaskSet TaskSet { get; }
            ITaskContext Context { get; }
        }

        private int mOrder;
        private readonly ITaskSet mTaskSet;
        private readonly ITaskContext mContext;

        protected TaskBase(ITaskSet taskSet, ITaskContext parentContext)
        {
            mTaskSet = taskSet;
            mContext = parentContext;
        }

        protected void MakeAfter(int order)
        {
            if (mOrder <= order)
            {
                mOrder = order + 1;
            }
        }

        void ITaskBaseView.MakeAfter(int order)
        {
            MakeAfter(order);
        }

        /// <summary>
        /// Приоритет задачи. 
        /// 0 - максимальный приоритет.
        /// Все задачи с одинаковым приоритетом могут исполняться конкурентно.
        /// Менее приоритетные задачи ожидают исполнения более приоритетных.
        /// </summary>
        public int Order
        {
            get { return mOrder; }
        }

        protected ITaskSet TaskSet
        {
            get { return mTaskSet; }
        }

        protected ITaskContext Context
        {
            get { return mContext; }
        }

        ITaskSet ITaskBaseView.TaskSet
        {
            get { return mTaskSet; }
        }

        ITaskContext ITaskBaseView.Context
        {
            get { return mContext; }
        }

        /// <summary>
        /// Выполняет работу
        /// </summary>
        /// <returns> TRUE - сигнализирует об успешности выполнения. </returns>
        public virtual Task<bool> DoWork()
        {
            return Task.FromResult(true);
        }

        /// <summary>
        /// Откатывает выполненную работу (если нужно). 
        /// Вызывается если DoWork() вернул FALSE
        /// </summary>
        public virtual Task Rollback()
        {
            return Task.FromResult<object>(null);
        }
        
        protected void Warning(string text)
        {
            Console.WriteLine("WARNING: " + text);
        }

        #region ITaskControl

        protected TTask Append<TTask>(TTask task) where TTask : TaskBase
        {
            task.MakeAfter(Order);
            mTaskSet.Add(task);
            return task;
        }

        public ITask<TResult> NextTask<TResult>(Task<TResult> task)
        {
            return Append(new AsyncTask<TResult>(mTaskSet, Context, task));
        }

        public ITask NextAction(Action<ITaskContext> action)
        {
            return Append(new ActionTask(mTaskSet, Context, action));
        }

        public ITask NextAction<TParam1>(Action<ITaskContext, TParam1> action, TParam1 param1)
        {
            return Append(new ActionTask<TParam1>(mTaskSet, Context, action, param1));
        }

        public ITask<TResult> NextFunction<TResult>(Func<ITaskContext, TResult> function)
        {
            return Append(new FunctionTask<TResult>(mTaskSet, Context, function));
        }

        public ITask<TResult> NextFunction<TParam1, TResult>(Func<ITaskContext, TParam1, TResult> function, TParam1 param1)
        {
            return Append(new FunctionTask<TParam1, TResult>(mTaskSet, Context, function, param1));
        }

        public ITask<TResult> NextFunction<TParam1, TParam2, TResult>(Func<ITaskContext, TParam1, TParam2, TResult> function, TParam1 param1, TParam2 param2)
        {
            return Append(new FunctionTask<TParam1, TParam2, TResult>(mTaskSet, Context, function, param1, param2));
        }

        #endregion
    }
}