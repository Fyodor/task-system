﻿namespace TaskSystem
{
    interface ITaskContext
    {
        void Log(string message, params object[] list);
    }
}