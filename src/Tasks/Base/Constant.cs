﻿namespace TaskSystem
{
    class Constant<TResult> : ITaskResult<TResult>
    {
        private readonly int mOrder;
        private readonly TResult mResult;

        public Constant(int order, TResult result)
        {
            mOrder = order;
            mResult = result;
        }

        public int Order
        {
            get { return mOrder; }
        }

        public TResult Result
        {
            get { return mResult; }
        }
    }
}