﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TaskSystem
{
    interface ITaskSet
    {
        void Add(TaskBase task);
    }

    class TaskSet : ITaskSet
    {
        private readonly List<TaskBase> mSubTasks = new List<TaskBase>();
        private int mProcessedTasksNumber = 0;

        private bool mAppendable = true;

        public void Add(TaskBase task)
        {
            if (!mAppendable)
            {
                throw new InvalidOperationException();
            }

            mSubTasks.Add(task);
        }

        public async Task<bool> Process()
        {
            if (!mAppendable)
            {
                throw new InvalidOperationException();
            }
            mAppendable = false;

            int count = mSubTasks.Count;
            if (count > 0)
            {
                mSubTasks.Sort((l, r) => l.Order.CompareTo(r.Order));

                for (int i = 0; i < mSubTasks.Count; ++i)
                {
                    if (await mSubTasks[i].DoWork())
                    {
                        mProcessedTasksNumber += 1;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public async Task Rollback()
        {
            if (mAppendable)
            {
                throw new InvalidOperationException();
            }

            for (int i = mProcessedTasksNumber - 1; i >= 0; --i)
            {
                await mSubTasks[i].Rollback();
            }
        }
    }
}