﻿using System;

namespace TaskSystem
{
    class FunctionTask<TParam1, TResult> : BaseFunctionTask<TResult>
    {
        private Func<ITaskContext, TParam1, TResult> mFunction;
        private TParam1 mParam1;

        public FunctionTask(ITaskSet taskSet, ITaskContext context, Func<ITaskContext, TParam1, TResult> function, TParam1 param1)
            : base(taskSet, context)
        {
            if (function.Target != null)
            {
                throw new InvalidOperationException("Delegate must be static");
            }
            mFunction = function;
            mParam1 = param1;
        }

        protected override TResult Process()
        {
            return mFunction(Context, mParam1);
        }
    }
}