﻿using System;

namespace TaskSystem
{
    class FunctionTask<TParam1, TParam2, TResult> : BaseFunctionTask<TResult>
    {
        private Func<ITaskContext, TParam1, TParam2, TResult> mFunction;
        private TParam1 mParam1;
        private TParam2 mParam2;

        public FunctionTask(ITaskSet taskSet, ITaskContext context, Func<ITaskContext, TParam1, TParam2, TResult> function, TParam1 param1, TParam2 param2)
            : base(taskSet, context)
        {
            if (function.Target != null)
            {
                throw new InvalidOperationException("Delegate must be static");
            }
            mFunction = function;
            mParam1 = param1;
            mParam2 = param2;
        }

        protected override TResult Process()
        {
            return mFunction(Context, mParam1, mParam2);
        }
    }
}