﻿using System.Threading.Tasks;

namespace TaskSystem
{
    abstract class BaseFunctionTask<TResult> : SingleTask, ITask<TResult>
    {
        private readonly Task<TResult> mTask;

        protected BaseFunctionTask(ITaskSet taskSet, ITaskContext context)
            : base(taskSet, context)
        {
            mTask = new Task<TResult>(Process);
        }
        
        protected abstract TResult Process();

        public TResult Result
        {
            get
            {
                if (!mTask.IsCompleted)
                {
                    Warning("WARNING: Task is not finished");
                }
                return mTask.Result;
            }
        }

        public sealed override Task<bool> DoWork()
        {
            return Invoke();
        }

        public sealed override Task Rollback()
        {
            return base.Rollback();
        }

        private async Task<bool> Invoke()
        {
            mTask.Start();
            await mTask;
            return true;
        }
    }
}