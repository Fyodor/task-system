﻿using System;

namespace TaskSystem
{
    class FunctionTask<TResult> : BaseFunctionTask<TResult>
    {
        private Func<ITaskContext, TResult> mFunction;

        public FunctionTask(ITaskSet taskSet, ITaskContext context, Func<ITaskContext, TResult> function)
            : base(taskSet, context)
        {
            if (function.Target != null)
            {
                throw new InvalidOperationException("Delegate must be static");
            }
            mFunction = function;
        }

        protected override TResult Process()
        {
            return mFunction(Context);
        }
    }
}