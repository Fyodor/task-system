﻿using System.Threading.Tasks;

namespace TaskSystem
{
    class AsyncTask<TResult> : SingleTask, ITask<TResult>
    {
        private readonly Task<TResult> mTask;

        public AsyncTask(ITaskSet taskSet, ITaskContext context, Task<TResult> task)
            : base(taskSet, context)
        {
            if (task.Status != TaskStatus.Created)
            {
                throw new System.InvalidOperationException("AsyncTask must not be started");
            }
            mTask = task;
        }

        public TResult Result
        {
            get
            {
                if (!mTask.IsCompleted)
                {
                    Warning("WARNING: Task is not finished");
                }
                return mTask.Result;
            }
        }

        public sealed override Task<bool> DoWork()
        {
            return Process();
        }

        public override Task Rollback()
        {
            return base.Rollback();
        }

        private async Task<bool> Process()
        {
            mTask.Start();
            await mTask;
            return true;
        }
    }
}