﻿using System.Threading.Tasks;

namespace TaskSystem
{
    abstract class BaseActionTask : SingleTask, ITask
    {
        protected BaseActionTask(ITaskSet taskSet, ITaskContext context)
            : base(taskSet, context)
        {
        }

        protected abstract void Process();

        public sealed override Task<bool> DoWork()
        {
            return Invoke();
        }

        public sealed override Task Rollback()
        {
            return base.Rollback();
        }

        private async Task<bool> Invoke()
        {
            await Task.Factory.StartNew(Process);
            return true;
        }
    }
}