﻿using System;

namespace TaskSystem
{
    class ActionTask : BaseActionTask
    {
        private Action<ITaskContext> mAction;

        public ActionTask(ITaskSet taskSet, ITaskContext context, Action<ITaskContext> action)
            : base(taskSet, context)
        {
            if (action.Target != null)
            {
                throw new InvalidOperationException("Delegate must be static");
            }
            mAction = action;
        }

        protected override void Process()
        {
            mAction(Context);
        }
    }
}