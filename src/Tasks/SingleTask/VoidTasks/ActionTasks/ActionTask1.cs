﻿using System;

namespace TaskSystem
{
    class ActionTask<TParam1> : BaseActionTask
    {
        private Action<ITaskContext, TParam1> mAction;
        private TParam1 mParam1;

        public ActionTask(ITaskSet taskSet, ITaskContext context, Action<ITaskContext, TParam1> action, TParam1 param1)
            : base(taskSet, context)
        {
            if (action.Target != null)
            {
                throw new InvalidOperationException("Delegate must be static");
            }
            mAction = action;
            mParam1 = param1;
        }

        protected override void Process()
        {
            mAction(Context, mParam1);
        }
    }
}