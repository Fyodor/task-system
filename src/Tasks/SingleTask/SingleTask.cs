﻿namespace TaskSystem
{
    abstract class SingleTask : TaskBase
    {
        protected SingleTask(ITaskSet taskSet, ITaskContext parentContext)
            : base(taskSet, parentContext)
        {
        }
    }
}