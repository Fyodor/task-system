﻿using System;
using TaskSystem;

namespace task_system
{
    class SagaA : Saga
    {
        private int mParam;

        public SagaA(int pr)
        {
            mParam = pr;
        }

        public override ITaskResult<bool> Process()
        {
            Console.WriteLine("A" + mParam);
            return Constant(true);
        }

        public override void Rollback()
        {
            // DO NOTHING
        }
    }

    class Tmp : Saga
    {
        public override ITaskResult<bool> Process()
        {
            ITask<bool> res = NextFunction(Check);

            {
                var a1 = If(res).SubSaga(new SagaA(1));
                If(res).WaitFor(a1).SubSaga(new SagaA(2));
            }
            {
                var b = IfNot(res).NextFunction(B);
                var c = IfNot(res).NextAction(C);
                IfNot(res).WaitFor(b, c).NextAction(D);
            }

            return Constant(true);
        }

        public override void Rollback()
        {
            // DO NOTHING
        }

        private static bool Mod5(ITaskContext context, int val)
        {
            return val % 5 == 0;
        }

        private static bool Check(ITaskContext context)
        {
            bool result = (new Random().Next() % 2 == 0);
            if (result)
            {
                Console.WriteLine("I Want to see 'A1A2'...");
            }
            else
            {
                Console.WriteLine("I want to see 'BCD' or 'CBD'...");
            }
            return result;
        }

        private static int B(ITaskContext context)
        {
            Console.WriteLine("B");
            return 123;
        }

        private static void C(ITaskContext context)
        {
            Console.WriteLine("C");
        }

        private static void D(ITaskContext context)
        {
            Console.WriteLine("D");
        }
    }

    /*
    class FiboCalculator : SagaTask<int>
    {
        private int mN;
        private ITaskResult<int> 

        public FiboCalculator(int n)
        {
            this.mN = n;
        }

        protected override IResult<int> Process(out IResult<bool> succeed)
        {
            succeed = Constant(true);

            if (mN <= 1)
            {
                return Constant(1);
            }

            var f1 = SubSaga(new FiboCalculator(mN - 1));
            var f2 = SubSaga(new FiboCalculator(mN - 2));

            ITaskFullControl waiter = WaitFor(f1, f2);
            ITask<int> sumRes = waiter.NextFunction(Sum, f1, f2);

            sumRes.NextAction(Print, sumRes);
            return sumRes;
        }

        protected override void Rollback()
        {
            // DO NOTHING
        }

        static int Sum(ITaskContext context, IResult<int> a, IResult<int> b)
        {
            return a.Result + b.Result;
        }

        static void Print(ITaskContext context, IResult<int> n)
        {
            Console.WriteLine(n.Result + " at thread #" + System.Threading.Thread.CurrentThread.ManagedThreadId);
        }

        public static int Check(int x)
        {
            if (x <= 1)
            {
                return 1;
            }
            int n1 = 1;
            int n2 = 1;

            for (int i = 2; i <= x; ++i)
            {
                int n = n1 + n2;
                n2 = n1;
                n1 = n;
            }
            return n1;
        }
    }
    */
    class Program
    {
        static void Main(string[] args)
        {
            {
                var check = new SagaTask(new Tmp());
                bool res =check.RunAsRoot().Result;
            }

            Console.WriteLine("\n");

            //{
            //    var fib = new FiboCalculator(6);
            //    fib.RunAsRoot();
            //    int res = fib.Result;
            //    Console.WriteLine("Result: {0} True={1}", res, FiboCalculator.Check(6));
            //}


            Console.ReadLine();
        }
    }
}
